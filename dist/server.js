"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
/*ROTAS*/
const auth_1 = __importDefault(require("./routes/auth"));
// import coursesRoutes from './routes/courses';
// import homeRoutes from './routes/home';
// import testRoutes from './routes/tests';
// import usersRoutes from './routes/users';
const app = express_1.default();
// import expressHandle from 'express-handlebars';
// import bodyParser from 'body-parser';
// import path from 'path';
// import session from 'express-session';
// import flash from 'connect-flash';
// import cookie from 'cookie-parser';
// import passport from 'passport';
// require('./config/auth')(passport);
// app.use(cookie());
// app.use(session({
//     secret: 'dwdjk#n152478D4DSFF4&bd!vy&',
//     resave: true,
//     saveUninitialized: true
// }));
// app.use(passport.initialize());
// app.use(passport.session());
// app.use(flash());
// app.use((req, res, next) => {
//     res.locals.success_msg = req.flash('success_msg');
//     res.locals.error_msg = req.flash('error_msg');
//     res.locals.error = req.flash('error');
//     res.locals.user = req.user || null;
//     next();
// });
// app.use(express.static(path.join(__dirname, 'static')));
// app.engine('handlebars', expressHandle({ defaultLayout: 'default' }));
// app.set('view engine', 'handlebars');
// app.use(express.json());
// app.use(bodyParser.urlencoded({ extended: true }))
// app.use(bodyParser.json());
// app.use([authRoutes, coursesRoutes, homeRoutes, usersRoutes, testRoutes]);
// //Not  Found
// app.use((req, res, next) => {
//     const error = new Error('Not Found');
//     error.status = 404;
//     next(error);
// });
// //catch all
// app.use((error, req, res, next) => {
//     res.status(error.status || 500);
//     res.json({ error: error.message });
// });
app.use(auth_1.default);
app.listen(3000, () => {
    console.log('Server is running');
});
//# sourceMappingURL=server.js.map